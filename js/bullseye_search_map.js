/**
 * js/bullseye_search_map.js
 * JavaScript for the Google Maps integration
 *
 * ajh 2012-10-02
 */

var bullseyeSearchMap = (function($) {
  var map;
  var bounds;
  var geocoder;
  var markers = [];
  var infoWindow;

  function fmtAddress(marker, bHtml) {
    var addr = '';

    if (marker.Address1) {
        addr = marker.Address1 + (bHtml ? '<br/>' : ', ');
    }
    if (marker.Address2) {
        addr += marker.Address2 + (bHtml ? '<br/>' : ', ');
    }
    if (marker.City) {
        addr += marker.City + ', ';
    }
    if (marker.State) {
        addr += marker.State + ' ';
    }
    if (marker.PostCode) {
        addr += marker.PostCode + ' ';
    }
    if (marker.CountryCode && !bHtml) {
      addr += marker.CountryCode;
    }
    return addr;
  }

  function createMarker(latlng, locId, name, addr, iMarkerNum) {
      //console.log("Creating marker for " + name);
      var html = "<strong>" + name + "</strong><address>" + addr + "</address>";
      var marker = new google.maps.Marker({ map: map, position: latlng });

      marker.setValues({ type: "point", id: locId });

      google.maps.event.addListener(marker, 'click', function () {
          infoWindow.setContent(html);
          infoWindow.open(map, marker);
      });
      markers.push(marker);
  }

  function clearLocations() {
      if (infoWindow)
          infoWindow.close();
      for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
      }
      markers.length = 0;
  }

  function geocodeAndCreateMarker(myMarker, i) {
    var fmtAddr = fmtAddress(myMarker, false);
    //console.log('geocoding addr ' + fmtAddr);
    geocoder.geocode(
      {'address': fmtAddr},
      function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          //console.log('returning ' + results[0].geometry.location);
          var locId = myMarker.Id;
          var name = myMarker.Name;
          var addrHtml = fmtAddress(myMarker, true);

          var latlng = results[0].geometry.location;
          createMarker(latlng, locId, name, addrHtml, i);
          bounds.extend(latlng);
          map.fitBounds(bounds);  // we have to do this here too, since this is async...
        } else {
          //console.log("Geocode failed. status: " + status);
        }
      }
    );
  }

return {
  init_map: function (mapCanvasId) {
    if (map != undefined) {
      markers = [];
      //return;
    }

    //console.log("running init_map(" + mapCanvasId + ")");
    geocoder = new google.maps.Geocoder();

    //var myLatlng = new google.maps.LatLng(40.764015, -73.982797); // NYC
    var myOptions = {
      zoom: 8,
      //center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var canvas = document.getElementById(mapCanvasId);
    //console.log(canvas);
    $(canvas).fadeIn();
    map = new google.maps.Map(canvas, myOptions);
    //google.maps.event.trigger(map, "resize");

    infoWindow = new google.maps.InfoWindow();
    //add_markers();
    //console.log("end of init_map");
  },

  add_markers: function (myMarkers) {
      //console.log("running add_markers");
      if (map == undefined)
          return;

      clearLocations();
      if (!myMarkers) return;
      if (myMarkers.length == 0) return;

      bounds = new google.maps.LatLngBounds();

      var i = 0;
      while (i < myMarkers.length) {
          //console.log(myMarkers[i]);
          if (myMarkers[i].Latitude == 0 || myMarkers[i].Longitude == 0) {
            geocodeAndCreateMarker(myMarkers[i], i);
          } else {
            var locId = myMarkers[i].Id;
            var name = myMarkers[i].Name;
            var addr = fmtAddress(myMarkers[i], true);

            var latlng = new google.maps.LatLng(myMarkers[i].Latitude, myMarkers[i].Longitude);
            createMarker(latlng, locId, name, addr, i);
            bounds.extend(latlng);
          }
          i++;
      }

      map.fitBounds(bounds);
  },

};

})(jQuery);
