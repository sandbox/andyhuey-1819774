/**
 * js/bullseye_search_form_stuff.js
 * JavaScript to show/hide stuff on the search form
 *
 * ajh 2012-10-12
 */

(function ($) {
  Drupal.behaviors.bullseyeSearchFormStuff = {
    attach: function(context) {
      // set it up to run on drop-down change
      $('select[name="country"]').change(function() {
        var countryId = $(this).val();
        setupForm(countryId);
      });
      // run it once
      var countryId = $('select[name="country"]').val();
      if (!countryId) {
        // if it's hidden:
        var countryId = $('input[name="country"]').val();
      }
      setupForm(countryId);
    },
  };

  function setupForm(countryId) {
      //console.log("setupForm(" + countryId + ")");
      if (countryId == 1 || countryId == 2) {
        $('.form-item-radius').show('fast');
        $('.form-item-location').show('fast');
      } else {
        $('.form-item-radius').hide('fast');
        $('.form-item-location').hide('fast');
      }
      var locPrompt = false;
      if (countryId == 1) {
        locPrompt = Drupal.t('Enter city, state OR zip code');
      } else if (countryId == 2) {
        locPrompt = Drupal.t('Enter city, province, OR postal code');
      }
      if (locPrompt) {
        $('.form-item-location > div.description').html(locPrompt);
      }
  }

})(jQuery);
