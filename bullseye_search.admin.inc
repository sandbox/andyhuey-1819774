<?php

/**
 * @file
 * Bullseye search admin menu callbacks.
 */

include_once 'bullseye_util.php';

/**
 * Admin settings form.
 */
function bullseye_search_admin_settings_form() {
  $form = array();
  $form['bullseye_search_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Bullseye Client ID'),
    '#default_value' => variable_get('bullseye_search_client_id'),
    '#size' => 12,
    '#maxlength' => 4,
    '#description' => t('Enter your numeric Bullseye client ID here.'),
    '#required' => TRUE,
  );
  $form['bullseye_search_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Bullseye Search API Key'),
    '#default_value' => variable_get('bullseye_search_api_key'),
    '#size' => 40,
    '#maxlength' => 36,
    '#description' => t('Enter your Bullseye search API key here.'),
    '#required' => TRUE,
  );
  // Default to production.
  $form['bullseye_search_server'] = array(
    '#type' => 'radios',
    '#title' => t('Server'),
    '#default_value' => variable_get('bullseye_search_server', 0),
    '#description' => t('Use the production server, unless you have been asked to use the staging server.'),
    '#options' => array(
      0 => t('Production'),
      1 => t('Staging'),
    ),
  );
  // US should be checked by default.
  $form['bullseye_search_countries'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Countries'),
    '#default_value' => variable_get('bullseye_search_countries', array(
      1 => 1,
      2 => 0,
      4 => 0)),
    '#description' => t('Select the countries you would like to expose on the form.'),
    '#options' => array(
      1 => t('United States'),
      2 => t('Canada'),
      4 => t('Other Countries'),
    ),
    '#required' => TRUE,
  );
  $form['bullseye_search_show_categories'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Categories'),
    '#default_value' => variable_get('bullseye_search_show_categories', 0),
    '#description' => t('Check this box to show a category drop-down on your search page.') .
    '<br/>' . t('(You can configure categories in your Bullseye account setup.)'),
  );
  $form['bullseye_search_show_map'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Map'),
    '#default_value' => variable_get('bullseye_search_show_map', 0),
    '#description' => t('Check this box if you would like to show a Google map along with your search results.'),
  );
  $form['bullseye_search_ip_loc_detect'] = array(
    '#type' => 'checkbox',
    '#title' => t('IP Location Detection'),
    '#default_value' => variable_get('bullseye_search_ip_loc_detect', 0),
    '#description' => t('Enabling this option will cause the store locator to attempt to run a search, on initial page load,') .
    '<br/>' . t("based on the user's location, as determined by IP address."),
  );

  $ssform = system_settings_form($form);
  $ssform['#submit'][] = 'bullseye_search_admin_settings_form_submit';
  return $ssform;
}

/**
 * Form validation for the setings form.
 */
function bullseye_search_admin_settings_form_validate($form, &$form_state) {
  $client_id = $form_state['values']['bullseye_search_client_id'];
  if (!is_numeric($client_id)) {
    form_set_error('bullseye_search_client_id', t('Client id must be numeric.'));
  }
  elseif ($client_id <= 0 || $client_id > 9999) {
    form_set_error('bullseye_search_client_id',
      t('Client id must be greater than zero and less than 9999.')
    );
  }

  $api_key = $form_state['values']['bullseye_search_api_key'];
  if (!BullseyeUtil::isValidGuid($api_key)) {
    form_set_error('bullseye_search_api_key',
      t('API key must be a valid GUID, for example, 12345678-90AB-CDEF-0123-4567890ABCDE.')
    );
  }

  $countries = $form_state['values']['bullseye_search_countries'];
  if ($countries[1] == 0 && $countries[2] == 0 && $countries[4] == 0) {
    form_set_error('bullseye_search_countries', t('You must select at least one country.'));
  }
}

/**
 * Submit hook for the settings form.
 */
function bullseye_search_admin_settings_form_submit($form, &$form_state) {
  // Clear any existing lists from cache.
  cache_clear_all('bullseye_search_country_list', 'cache');
  cache_clear_all('bullseye_search_category_list', 'cache');
}
