<?php

/**
 * @file
 * Bullseye search API web service calls
 * See http://api.bullseyelocations.com/content/rest-search-web-service for reference.
 */
class BullseyeSearchSvc {
  const PAGE_SIZE = 10; 
  private static $prod_server = "http://leadmanagerws.electricvine.com";
  private static $stg_server = "http://leadmanagerws.staging.electricvine.com";

  private $server;
  private $clientId;
  private $searchApiKey;
  private $showMap;
  public $lastError;
  
  // Bullseye country IDs
  const COUNTRY_US = 1;
  const COUNTRY_CA = 2;

  /**
   * constructor.
   */
  function __construct() {
    $this->clientId = variable_get('bullseye_search_client_id');
    $this->searchApiKey = variable_get('bullseye_search_api_key');
    $this->showMap = variable_get('bullseye_search_show_map');

    $server_setting = variable_get('bullseye_search_server');
    if ($server_setting == 0) {
      $this->server = self::$prod_server;
    }
    else {
      $this->server = self::$stg_server;
    }

    $lastError = NULL;
  }

  /**
   * Call the Bullseye search method and return results.
   */
  public function runSearch($countryId, $radius, $loc, $categoryId, $ip_address, $startIndex) {
    // $loc is an associative array returned from BullseyeUtil.parseLocationText.

    $searchParams = array(
      "ClientId" => $this->clientId,
      "ApiKey" => $this->searchApiKey,
      "CountryId" => $countryId,
      "Radius" => $radius,
      "StartIndex" => $startIndex,
      "PageSize" => self::PAGE_SIZE,
      // TODO: "SearchSourceName" => "Web",
    );
    
    $ip_address = filter_var($ip_address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    if ($ip_address) {
      $searchParams['UserIPAddress'] = $ip_address;
    }

    // if we're showing a map, try to get geocodes from Bullseye.
    if ($this->showMap) {
      $searchParams['ReturnGeocode'] = 'TRUE';
    }

    if ($loc['postal_code']) {
      $searchParams['PostalCode'] = $loc['postal_code'];
    }
    elseif ($loc['city']) {
      $searchParams['City'] = $loc['city'];
      $searchParams['State'] = $loc['province'];
    }

    if ($categoryId > 0) {
      $searchParams['CategoryIds'] = $categoryId;
    }

    //dpm($searchParams);
    $myUrl = $this->server . "/RestSearch.svc/DoSearch2?" . http_build_query($searchParams);

    $method = "GET";

    $myHeaders = array(
      'Accept: application/json',
      'Content-type: application/json',
    );

    $handle = curl_init();
    curl_setopt_array($handle, array(
        CURLOPT_URL => $myUrl,
        CURLOPT_HTTPHEADER => $myHeaders,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
      ));

    $response = curl_exec($handle);
    $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

    //dpm("return code = $code");
    //print "response = <br/> $response <br/><br/>";
    $myRespArr = json_decode($response, TRUE);
    //print "<pre>";
    //var_dump($myRespArr);
    //print "</pre>";

    $this->setLastError($code);
    return $myRespArr;
  }

  /**
   * Get array of categories.
   *
   * @return
   *      array, each element is an associative array of info for a single category.
   */
  private function getCategories() {
    $myUrl = $this->server . "/RestSearch.svc/GetCategories?";
    $myUrl .= http_build_query(array(
        "ClientId" => $this->clientId,
        "ApiKey" => $this->searchApiKey,
      ));

    $myHeaders = array(
      'Accept: application/json',
      'Content-type: application/json',
    );

    $handle = curl_init();

    curl_setopt_array($handle, array(
        CURLOPT_URL => $myUrl,
        CURLOPT_HTTPHEADER => $myHeaders,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
      ));

    $response = curl_exec($handle);
    $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

    // return code should be '200'
    //print "return code = $code <br/>";

    $myRespArr = json_decode($response, TRUE);
    $this->setLastError($code);
    //var_dump($myRespArr);
    return $myRespArr;
  }

  /**
   * Returns an array of top-level categories,
   * where the key is the Bullseye cat id, and the value is the category name.
   */
  public function getTopLevelCategoryArray() {
    $categories = $this->getCategories();
    if ($this->lastError != 'ok') {
      return NULL;
    }
    $filtCats = array();
    $catZero = array_filter($categories, array($this, 'catZero'));

    foreach ($catZero as $x) {
      $filtCats[$x['CategoryId']] = $x['CategoryName'];
    }
    natsort($filtCats);
    $filtCats = array(-1 => 'All') + $filtCats;

    return $filtCats;
  }

  /**
   * Get categories and arrange into parent/child order.
   * The resulting array could be used to populate a drop-down of categories.
   */
  public function getTwoLevelCategoryArray() {
    $categories = $this->getCategories();
    if ($this->lastError != 'ok') {
      return NULL;
    }
    $filtCats = array();
    $catZero = array_filter($categories, array($this, 'catZero'));
    $catOne = array_filter($categories, array($this, 'catOne'));

    usort($catZero, array($this, 'catNameSort'));

    foreach ($catZero as $x) {
      $parentId = $x['CategoryId'];
      $childCats = array();
      foreach ($catOne as $y) {
        if ($y['ParentCategoryId'] == $parentId) {
          $childCats[$y['CategoryId']] = $y['CategoryName'];
        }
      }
      if (count($childCats) > 0) {
        // sort the child array, add an 'all' entry to it, then add it to the parent array.
        natsort($childCats);
        $childCats = array($x['CategoryId'] => '-All: ' . $x['CategoryName']) + $childCats;
        $filtCats[$x['CategoryName']] = $childCats;
      }
      else {
        // no child array, just add to top-level array.
        $filtCats[$x['CategoryId']] = $x['CategoryName'];
      }
    }
    $filtCats = array(-1 => '-All-') + $filtCats;
    return $filtCats;
  }

  private static function catZero($cat) {
    return $cat['Depth'] == 0 ? 1 : 0;
  }
  private static function catOne($cat) {
    return $cat['Depth'] == 1 ? 1 : 0;
  }

  private static function catNameSort($catA, $catB) {
    return strnatcasecmp($catA['CategoryName'], $catB['CategoryName']);
  }

  /**
   * Return the client's country list in the form of an array
   * with key = Bullseye ID and value = name.
   * Leave US & Canada out.
   */
  public function getCountryArray() {

    $country_list = $this->getCountryList();
    if ($this->lastError != 'ok') {
      return NULL;
    }

    $country_array = array();
    foreach ($country_list as $key => $val) {
      if (is_int($key) && $val['Id'] != self::COUNTRY_US && $val['Id'] != self::COUNTRY_CA) {
        $country_array[$val['Id']] = $val['Name'];
      }
    }
    return $country_array;
  }

  /**
   * Call the Bullseye GetCountryList method and return results.
   */
  private function getCountryList() {
    $searchParams = array(
      "ClientId" => $this->clientId,
      "ApiKey" => $this->searchApiKey,
    );

    $myUrl = $this->server . "/RestSearch.svc/GetCountryList?" . http_build_query($searchParams);

    $method = "GET";

    $myHeaders = array(
      'Accept: application/json',
      'Content-type: application/json',
    );

    $handle = curl_init();
    curl_setopt_array($handle, array(
        CURLOPT_URL => $myUrl,
        CURLOPT_HTTPHEADER => $myHeaders,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
      ));

    $response = curl_exec($handle);
    $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

    //dpm("return code = $code");
    //print "response = <br/> $response <br/><br/>";
    $myRespArr = json_decode($response, TRUE);
    //print "<pre>";
    //var_dump($myRespArr);
    //print "</pre>";

    $this->setLastError($code);

    return $myRespArr;
  }

  /**
   * Call the Bullseye GetLocationByIP method and return results.
   */
  public function getLocationByIP($ipAddress) {
    $searchParams = array(
      "ClientId" => $this->clientId,
      "ApiKey" => $this->searchApiKey,
      "IPAddress" => $ipAddress,
    );

    $myUrl = $this->server . "/RestSearch.svc/GetLocationByIP?" . http_build_query($searchParams);

    $method = "GET";

    $myHeaders = array(
      'Accept: application/json',
      'Content-type: application/json',
    );

    $handle = curl_init();
    curl_setopt_array($handle, array(
        CURLOPT_URL => $myUrl,
        CURLOPT_HTTPHEADER => $myHeaders,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
      ));

    $response = curl_exec($handle);
    $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

    $this->setLastError($code);
    if ($this->lastError != 'ok') {
      return NULL;
    }

    $myRespArr = json_decode($response, TRUE);
    return $myRespArr;
  }

  /**
   * Set the 'lastError' variable, based on the http response code.
   */
  private function setLastError($code) {
    switch ($code) {
      case 0:
        // means that the site couldn't be reached, or something else odd...
        $this->lastError = 'Unknown error';
        break;

      case 200:
        // all is well...
        $this->lastError = 'ok';
        break;

      case 401:
        // means that the client ID / API key combination is invalid.
        // (TotalResults = -1)
        $this->lastError = 'Unauthorized';
        break;

      case 403:
        // means that this IP is blocked.
        // (TotalResults = -2)
        $this->lastError = 'Forbidden';
        break;

      case 400:
        // means that one of the parameters (lat/lon probably) could not be parsed
        // (TotalResults = -3)
        $this->lastError = 'Bad Request';
        break;
    }
  }
}

