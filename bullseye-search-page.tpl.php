<?php
/**
 * @file
 * Themes the Bullseye search page.
 */
?>

<?php
  print render($form);

  print "<div id='bullseye-search-map-canvas'></div>";

  print render($results);

  $module_path = drupal_get_path('module', 'bullseye_search');
  print "<br/><a class='bullseye-search-logo' href='http://www.bullseyelocations.com' target='_blank'>";
  print "Powered by <img src='$module_path/images/bullseye_logo.png' alt='Powered by Bullseye'>";
  print "</a>";	
?>
