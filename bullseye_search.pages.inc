<?php

/**
 * @file
 * Bullseye search page callbacks.
 */

include_once 'bullseye_util.php';
include_once 'bullseye_search_svc.php';

/**
 * Main callback function for /storelocator page.
 */
function bullseye_search_page_callback() {
  $module_path = drupal_get_path('module', 'bullseye_search');
  drupal_add_js('https://maps.google.com/maps/api/js?sensor=false');
  drupal_add_js($module_path . '/js/bullseye_search_map.js');
  drupal_add_js($module_path . '/js/bullseye_search_form_stuff.js');
  drupal_add_css($module_path . '/css/bullseye_search.css');
  drupal_add_library('system', 'drupal.ajax');

  // If there is no saved configuration yet, show message and exit.
  // Do not render form.
  $client_id = variable_get('bullseye_search_client_id');
  if (!is_numeric($client_id)) {
    drupal_set_message(t('You have not yet configured the store locator.'), 'error');
    return t('Please go to <a href="@config-link">/admin/config/services/bullseye</a> to configure your store locator.',
      array('@config-link' => url('admin/config/services/bullseye'))
    );
  }

  $bs_search_form = drupal_get_form('bullseye_search_search_form');
  $result_output = array();

  if (isset($_SESSION['bs_result_list'])) {
    $result_list = $_SESSION['bs_result_list'];
    $total_results = $_SESSION['bs_total_results'];
    $start_index = isset($_SESSION['bs_start_index']) ? $_SESSION['bs_start_index'] : 0;
    $result_output = _bullseye_search_create_result_list_output($result_list, $total_results, $start_index);
  }

  $output = array(
    'form' => $bs_search_form,
    'results' => $result_output,
  );
  return theme('bullseye-search-page', $output);
}

/**
 * Ajax callback function for prev/next links.
 */
function bullseye_search_load_results_callback($ajax, $start_index) {
  $is_ajax = $ajax === 'ajax';
  if (!$is_ajax) {
    // drupal_set_message('Cannot call this function without ajax.', 'error');
    watchdog('bullseye_search', 'bullseye_search_load_results_callback called without ajax.');
    return;
  }

  if (!isset($_SESSION['bs_search_params'])) {
    watchdog('bullseye_search', 'bullseye_search_load_results_callback: bs_search_params missing.');
    return;
  }
  $params = $_SESSION['bs_search_params'];

  $my_resp_arr = bullseye_search_run_search(
    $params['country'], $params['radius'], $params['loc'], $params['category'], $start_index
  );
  if (!$my_resp_arr) {
    return;
  }

  $result_list = $my_resp_arr['ResultList'];
  $total_results = $my_resp_arr['TotalResults'];
  $result_output = _bullseye_search_create_result_list_output($result_list, $total_results, $start_index);

  $commands = array();
  $commands[] = ajax_command_replace('#bullseye-search-results-list', drupal_render($result_output));
  $commands[] = ajax_command_invoke('#bullseye-search-results-list', 'hide');
  $commands[] = ajax_command_invoke('#bullseye-search-results-list', 'fadeIn', array(200));

  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}

/**
 * Assemble and return the result output render array.
 */
function _bullseye_search_create_result_list_output($result_list, $total_results, $start_index) {
  $result_output = array();
  $show_map = variable_get('bullseye_search_show_map');
  // Used for map markers.
  $my_markers = array();
    
  // dpm($result_list);
  unset($_SESSION['bs_result_list']);
  unset($_SESSION['bs_total_results']);

  $result_output['#prefix'] = '<div id="bullseye-search-results-list">';
  $result_output['#suffix'] = '</div>';

  foreach ($result_list as $res) {
    $res_markup = '<div class="bullseye-search-address">';
    $res_markup .= '<strong>' . $res['Name'] . '</strong><br/>';
    if ($res['Address1']) {
      $res_markup .= $res['Address1'] . '<br/>';
    }
    $csz = BullseyeUtil::fmtCityStateZip($res['City'], $res['State'], $res['PostCode']);
    if ($csz) {
      $res_markup .= $csz . '<br/>';
    }
    if ($res['PhoneNumber']) {
      $res_markup .= $res['PhoneNumber'] . '<br/>';
    }
    $res_markup .= '</div>';  // address
    
    $res_markup .= '<div class="bullseye-search-dist-map">';
    // Handle distance
    $distance = $res['Distance'];
    if ($distance < 1000) {
      if ($res['CountryCode'] == 'US') {
        $res_markup .= t('Distance:') . " $distance  mi<br/>";
      }
      else {
        $res_markup .= t('Distance:') . ' ' . BullseyeUtil::convertMilesToKilometers($distance) . ' km<br/>';
      }
    }

    $map_url = "http://maps.google.com/maps?" . http_build_query(array(
        'q' => $res['Address1'] . ' ' . $csz . ', ' . $res['CountryCode'],
      ));
    $res_markup .= l('Map it', $map_url) . '<br/>';
    $res_markup .= '</div>';  // dist-map
    
    $result_output[] = array(
      '#prefix' => '<div class="bullseye-search-pager">',
      '#suffix' => '</div>',
      '#markup' => $res_markup,
    );

    if ($show_map) {
      $my_markers[] = array(
        'Id' => check_plain($res['Id']),
        'Name' => check_plain($res['Name']),
        'Address1' => check_plain($res['Address1']),
        'Address2' => check_plain($res['Address2']),
        'City' => check_plain($res['City']),
        'State' => check_plain($res['State']),
        'PostCode' => check_plain($res['PostCode']),
        'CountryCode' => check_plain($res['CountryCode']),
        'Latitude' => check_plain($res['Latitude']),
        'Longitude' => check_plain($res['Longitude']),
      );
    }
  }

  if ($show_map) {
    // Render out the JS marker array and init the map.
    $res_markup = 'var myMarkers = ' . json_encode($my_markers) . '; ' .
      'bullseyeSearchMap.init_map("bullseye-search-map-canvas"); ' .
      'bullseyeSearchMap.add_markers(myMarkers); ';
      
    $result_output[] = array(
      '#markup' => $res_markup,
      '#prefix' => '<script type="text/javascript">',
      '#suffix' => '</script>',
    );
  }

  // Pager.
  $res_markup = '';
  if ($start_index > 0) {
    // We need a 'previous' pager.
    $prev_index = $start_index - BullseyeSearchSvc::PAGE_SIZE;
    $res_markup .= '<span id="bullseye-search-prev">'
      . l('< Prev', "storelocator/load/nojs/$prev_index", array('attributes' => array('class' => 'use-ajax')))
      . '</span>';
  }
  if ($total_results > $start_index + BullseyeSearchSvc::PAGE_SIZE) {
    // We need a 'next' pager.
    $next_index = $start_index + BullseyeSearchSvc::PAGE_SIZE;
    $res_markup .= '<span class="bullseye-search-next">'
      . l('Next >', "storelocator/load/nojs/$next_index", array('attributes' => array('class' => 'use-ajax')))
      . '</span>';
  }
  if ($res_markup) {
    $result_output[] = array(
      '#markup' => $res_markup,
      '#prefix' => '<div class="bullseye-search-pager">',
      '#suffix' => '</div>',
    );
  }
  return $result_output;
}

/**
 * Returns the main Bullseye search form.
 */
function bullseye_search_search_form($form, &$form_state) {
  // Get any form values.
  $country = !empty($form_state['values']['country']) ? $form_state['values']['country'] : 0;
  $def_radius = !empty($form_state['values']['radius']) ? $form_state['values']['radius'] : 5;
  $def_location = !empty($form_state['values']['location']) ? $form_state['values']['location'] : '';
  $def_category = !empty($form_state['values']['category']) ? $form_state['values']['category'] : 0;

  // Set up the form.
  $form = array();

  // Set up a country drop-down.
  $b_need_other_countries = FALSE;
  $countries = variable_get('bullseye_search_countries');
  $country_opts = array();
  if ($countries[1] == 1) {
    $country_opts[1] = 'United States';
  }
  if ($countries[2] == 2) {
    $country_opts[2] = 'Canada';
  }
  if ($countries[4] == 4) {
    $b_need_other_countries = TRUE;
  }

  if ($b_need_other_countries) {
    // Get the country list.
    $country_list_obj = cache_get('bullseye_search_country_list', 'cache');
    if ($country_list_obj && $country_list_obj->data) {
      $country_list = $country_list_obj->data;
    }
    else {
      $country_list = bullseye_search_get_country_list();
      cache_set('bullseye_search_country_list', $country_list, 'cache', CACHE_TEMPORARY);
    }
    $country_opts += $country_list;
  }

  // If this is the first time in, try to do a search based on IP location.
  $b_force_submit = FALSE;
  $ip_location = NULL;
  $ip_country = NULL;
  $b_ip_detect = variable_get('bullseye_search_ip_loc_detect');
  if ($b_ip_detect && empty($form_state['values']) && empty($form_state['input']) && $form_state['submitted'] == 0) {
    $loc = bullseye_search_get_location_by_ip();
    if ($loc) {
      if ($loc['Country'] == 'US' && $countries[1] == 1) {
        $ip_country = 1;
        $ip_location = $loc['City'] . ', ' . $loc['State'];
        $b_force_submit = TRUE;
      }
      elseif ($loc['Country'] == 'CA' && $countries[2] == 2) {
        $ip_country = 2;
        $ip_location = $loc['City'] . ', ' . $loc['State'];
        $b_force_submit = TRUE;
      }
      elseif ($b_need_other_countries && array_key_exists($loc['CountryId'], $country_list)) {
        $ip_country = $loc['CountryId'];
        $def_location = '';
        $b_force_submit = TRUE;
      }
      //dpm($loc);
    }
    $parsed_loc = NULL;
    if ($b_force_submit && $ip_location != '') {
      $parsed_loc = BullseyeUtil::parseLocationText($ip_location);
      if (!$parsed_loc['valid']) {
        $b_force_submit = FALSE;
      }
    }
  }

  // Get the first item in the drop-down.
  reset($country_opts);
  $default_country = key($country_opts);

  if (!$country) {
    $country = $default_country;
  }

  $form['country'] = array(
    '#title' => t('Country'),
    '#type' => 'select',
    '#options' => $country_opts,
    '#default' => $country,
  );

  if ($ip_country) {
    $form['country']['#value'] = $ip_country;
  }

  // Hide the drop-down if there's only one country.
  if (count($country_opts) == 1) {
    $form['country']['#type'] = 'hidden';
    $form['country']['#value'] = $default_country;
  }

  $form['radius'] = array(
    '#title' => t('Radius'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array(5, 10, 25, 50, 100, 150)),
    '#default' => $def_radius,
  );

  if ($country == 1) {
    $loc_desc = t('Enter city, state OR zip code');
  }
  else {
    $loc_desc = t('Enter city, province, OR postal code');
  }
  $form['location'] = array(
    '#title' => t('Location'),
    '#description' => check_plain($loc_desc),
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 250,
    '#default' => $def_location,
  );

  if ($ip_location) {
    $form['location']['#value'] = $ip_location;
  }

  $form['category'] = array(
    '#title' => t('Category'),
  );

  $show_cats = variable_get('bullseye_search_show_categories');
  if ($show_cats) {
    $cat_list_obj = cache_get('bullseye_search_category_list', 'cache');
    if ($cat_list_obj && $cat_list_obj->data) {
      $cat_list = $cat_list_obj->data;
    }
    else {
      $cat_list = bullseye_search_get_category_list();
      cache_set('bullseye_search_category_list', $cat_list, 'cache', CACHE_TEMPORARY);
    }
    if ($cat_list) {
      $form['category']['#type'] = 'select';
      $form['category']['#options'] = $cat_list;
      $form['category']['#default'] = $def_category;
    }
  }
  else {
    $form['category']['#type'] = 'hidden';
    $form['category']['#value'] = -1;
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
  );

  // Run the search & save to session,
  // so that the page callback will see it and render results.
  if ($b_force_submit) {
    bullseye_search_run_and_save_to_session($ip_country, $def_radius, $parsed_loc, $def_category);
  }

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function bullseye_search_search_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $country = $values['country'];
  $locationText = $values['location'];
  if ($country == 1 || $country == 2) {
    $loc = BullseyeUtil::parseLocationText($locationText);
    if ($loc['valid'] == FALSE || $loc['country'] != $country) {
      // krumo($loc);
      if ($country == 1) {
        form_set_error('location', t('Please enter a valid city/state or zip code.'));
      }
      else {
        form_set_error('location', t('Please enter a valid city/province or postal code.'));
      }
    }
  }
}

/**
 * Run the search for the given parameters, and return the results.
 */
function bullseye_search_run_search($country, $radius, $loc, $categoryId, $start_index) {
  $ip_address = $_SERVER['REMOTE_ADDR'];
  $my_search_obj = new BullseyeSearchSvc();
  $my_resp_arr = $my_search_obj->runSearch($country, $radius, $loc, $categoryId, $ip_address, $start_index);

  if ($my_search_obj->lastError != 'ok') {
    drupal_set_message(t('There was an error processing your search:') . ' ' . $my_search_obj->lastError, 'error');
    watchdog('bullseye_search', 'search error: ' . $my_search_obj->lastError);
    return NULL;
  }
  if ($my_resp_arr['TotalResults'] <= 0) {
    drupal_set_message(t('No matching results were found.'), 'warning');
    return NULL;
  }

  return $my_resp_arr;
}

/**
 * Get the country list from Bullseye, and return the results.
 */
function bullseye_search_get_country_list() {
  $my_search_obj = new BullseyeSearchSvc();
  $my_resp_arr = $my_search_obj->getCountryArray();

  if ($my_search_obj->lastError != 'ok') {
    drupal_set_message(t('There was an error getting the country list:') . ' ' . $my_search_obj->lastError, 'error');
    watchdog('bullseye_search', 'getCountryList error: ' . $my_search_obj->lastError);
    return NULL;
  }
  return $my_resp_arr;
}

/**
 * Get the category list from Bullseye.
 */
function bullseye_search_get_category_list() {
  $my_search_obj = new BullseyeSearchSvc();
  //$my_resp_arr = $my_search_obj->getTopLevelCategoryArray();
  $my_resp_arr = $my_search_obj->getTwoLevelCategoryArray();

  if ($my_search_obj->lastError != 'ok') {
    drupal_set_message(t('There was an error getting the category list: ') . $my_search_obj->lastError, 'error');
    watchdog('bullseye_search', 'getCategoryList error: ' . $my_search_obj->lastError);
    return NULL;
  }
  return $my_resp_arr;
}

/**
 * Get the location for the current user's IP address.
 */
function bullseye_search_get_location_by_ip() {
  $ip_address = ip_address(); //$_SERVER['REMOTE_ADDR'];
  $my_search_obj = new BullseyeSearchSvc();
  $my_resp_arr = $my_search_obj->getLocationByIP($ip_address);

  if ($my_search_obj->lastError != 'ok') {
    // drupal_set_message(t('There was an error getting the location: ') . $my_search_obj->lastError, 'error');
    watchdog('bullseye_search', 'getLocationByIP error: ' . $my_search_obj->lastError);
    return NULL;
  }
  if ($my_resp_arr['ErrorMsg'] != '') {
    // drupal_set_message(t('There was an error getting the location: ') . $my_resp_arr['ErrorMsg'], 'error');
    watchdog('bullseye_search', 'getLocationByIP error: ' . $my_resp_arr['ErrorMsg']);
    return NULL;
  }
  return $my_resp_arr;
}

/**
 * Implements hook_form_submit().
 */
function bullseye_search_search_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  // dpm($values);
  $country = $values['country'];
  $locationText = $values['location'];
  $category = $values['category'];
  $loc = NULL;
  if ($country == 1 || $country == 2) {
    $loc = BullseyeUtil::parseLocationText($locationText);
    if ($loc['valid'] == FALSE) {
      // Shouldn't happen.
      form_set_error('location', t('Please enter a valid city/state or postal code.'));
      return;
    }
  }

  $country = $values['country'];
  $radius = $values['radius'];
  // Skip radius for countries other than US and Canada.
  if ($country > 2) {
    $radius = NULL;
  }

  $form_state['rebuild'] = TRUE;
  bullseye_search_run_and_save_to_session($country, $radius, $loc, $category);
}

/*
 * Run the search and save the results to session vars.
 */
function bullseye_search_run_and_save_to_session($country, $radius, $loc, $category) {
  $search_params = array(
    'country' => $country,
    'radius' => $radius,
    'loc' => $loc,
    'category' => $category,
  );

  //$form_state['rebuild'] = TRUE;
  $my_resp_arr = bullseye_search_run_search($country, $radius, $loc, $category, 0);
  if (!$my_resp_arr) {
    return;
  }

  // Save to session.
  $_SESSION['bs_total_results'] = $my_resp_arr['TotalResults'];
  $_SESSION['bs_result_list'] = $my_resp_arr['ResultList'];
  $_SESSION['bs_start_index'] = 0;
  $_SESSION['bs_search_params'] = $search_params;
}

