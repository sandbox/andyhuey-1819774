<?php

/**
 * @file
 * Bullseye utility class
 */

// we need a couple of things from the location module...
$be_util_location_path = drupal_get_path('module', 'location');
include_once (DRUPAL_ROOT . '/' . $be_util_location_path . '/supported/location.us.inc');
include_once (DRUPAL_ROOT . '/' . $be_util_location_path . '/supported/location.ca.inc');

class BullseyeUtil {
  // Bullseye country IDs
  const COUNTRY_US = 1;
  const COUNTRY_CA = 2;
  //const COUNTRY_INTL = 4;

  // two possible search types
  const ST_POSTAL_CODE = 1;
  const ST_CITY_ST = 2;

  /**
   * This tries to parse location text, and return an array with the info broken out.
   */
  public static function parseLocationText($strLocationText) {
    $loc = array();
    $loc['input_text'] = check_plain($strLocationText);
    $loc['valid'] = FALSE;
    $loc['search_type'] = NULL;
    $loc['country'] = NULL;
    $loc['postal_code'] = NULL;
    $loc['city'] = NULL;
    $loc['province'] = NULL;

    $loc = self::getSearchType($loc);

    if ($loc['search_type'] == self::ST_POSTAL_CODE) {
      if ($loc['country'] == self::COUNTRY_US) {
        if (drupal_strlen($strLocationText) > 5) {
          $loc['postal_code'] = drupal_substr($strLocationText, 0, 5);
        }
        else {
          $loc['postal_code'] = $strLocationText;
        }
        return $loc;
      }
      else {
        // canada
        $loc['postal_code'] = $strLocationText;
        return $loc;
      }
    }
    // not a postal code, probably city/st...
    $loc = self::parseCityState($loc);
    return $loc;
  }

  private static function isUSZip($strCityStZip) {
    // match on 5-digit zip or zip+4 (99999-9999)
    return preg_match("/^\d{5}$|^\d{5}-\d{4}$/", $strCityStZip);
    //return Regex.IsMatch(strCityStZip, @"^\d{5}-\d{4}$|^\d{5}$");
  }

  private static function isCanadaPostCode($strCityStZip) {
    // basic format is 'A9A 9A9'
    //return Regex.IsMatch(strCityStZip.ToUpper(), @"^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$");
    return preg_match("/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i", $strCityStZip);
  }

  /**
   * given the contents of the location text field,
   * determine if the user entered a postal code or city/state.
   */
  private static function getSearchType($loc) {
    $strLocationText = $loc['input_text'];

    if (self::isUsZip($strLocationText)) {
      $loc['search_type'] = self::ST_POSTAL_CODE;
      $loc['country'] = self::COUNTRY_US;
      $loc['valid'] = TRUE;
    }
    elseif (self::isCanadaPostCode($strLocationText)) {
      $loc['search_type'] = self::ST_POSTAL_CODE;
      $loc['country'] = self::COUNTRY_CA;
      $loc['valid'] = TRUE;
    }
    else {
      $loc['search_type'] = self::ST_CITY_ST;
    }

    return $loc;
  }

  /**
   * validates and parses a string that may contain city, state.
   * returns true on success.
   */
  private static function parseCityState($loc) {
    $strInput = $loc['input_text'];

    $loc['country'] = NULL;
    $loc['city'] = NULL;
    $loc['province'] = NULL;

    $aCitySt = array();

    if (strpos($strInput, ',') > 0) {
      $delimeter = ',';
    }
    elseif (strpos($strInput, ' ') > 0) {
      $delimeter = ' ';
    }
    else {
      $loc['valid'] = FALSE;
      return $loc;
    }

    $aCitySt = explode($delimeter, $strInput);
    if (count($aCitySt) < 2) {
      $loc['valid'] = FALSE;
      return $loc;
    }

    $strStateText = trim($aCitySt[count($aCitySt) - 1]);
    $strStateName = '';
    $ctryID = self::getStateInfo($strStateText, $strStateCode, $strStateName);

    if ($ctryID == 0) {
      //no match
      $loc['valid'] = FALSE;
      return $loc;
    }
    //assume the first part is the city
    $strCity = drupal_substr($strInput, 0, drupal_strlen($strInput) - drupal_strlen($strStateText));
    $strCity = trim(trim($strCity), ',');
    $loc['country'] = $ctryID;
    $loc['city'] = $strCity;
    $loc['province'] = $strStateCode;
    $loc['valid'] = TRUE;
    return $loc;
  }

  /**
   * Given a state abbreviation or full name,
   * return 1 if it's in the US, or 2 if it's a Canadian province.
   * Return zero if neither.
   * Fill in variables for state code & name.
   * Needs location.us.inc and location.ca.inc from location/supported.
   */
  private static function getStateInfo($stateText, &$strStateCode, &$strStateName) {
    $us_states = location_province_list_us();
    $ca_provinces = location_province_list_ca();
    $strStateCode = '';
    $strStateName = '';
    $stateText = strtoupper($stateText);
    if (array_key_exists($stateText, $us_states)) {
      $strStateCode = $stateText;
      $strStateName = $us_states[$stateText];
      return self::COUNTRY_US;
    }
    foreach ($us_states as $key => $value) {
      if (strcasecmp($value, $stateText) == 0) {
        $strStateCode = $key;
        $strStateName = $value;
        return self::COUNTRY_US;
      }
    }
    if (array_key_exists($stateText, $ca_provinces)) {
      $strStateCode = $stateText;
      $strStateName = $ca_provinces[$stateText];
      return self::COUNTRY_CA;
    }
    foreach ($ca_provinces as $key => $value) {
      if (strcasecmp($value, $stateText) == 0) {
        $strStateCode = $key;
        $strStateName = $value;
        return self::COUNTRY_CA;
      }
    }
    return 0;
  }

  /**
   * format city, state, zip & return.
   */
  public static function fmtCityStateZip($city, $state, $zip) {
    $output = $city;
    if ($state) {
      $output .= ", $state";
    }
    if ($zip) {
      $output .= " $zip";
    }
    return $output;
  }

  public static function isValidGuid($strGuid) {
    return preg_match('/^\{?[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}\}?$/i', $strGuid);
  }

  public static function convertMilesToKilometers($mi) {
    return round($mi * 1.60934721869, 2);
  }
}

// run with 'drush scr' to test...
//echo 'running tests...';
//$loc = BullseyeUtil::parseLocationText('08873');
//var_dump($loc);
//$loc = BullseyeUtil::parseLocationText('G1R 1S3');
//var_dump($loc);
//$loc = BullseyeUtil::parseLocationText('Quebec City QC');
//var_dump($loc);
//$loc = BullseyeUtil::parseLocationText('Somerset, NJ');
//var_dump($loc);
//$loc = BullseyeUtil::parseLocationText('foo bar');
//var_dump($loc);
//$loc = BullseyeUtil::parseLocationText('blech, zz');
//var_dump($loc);

