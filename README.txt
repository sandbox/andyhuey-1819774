----------------------
Bullseye Store Locator
----------------------

This module implements a Drupal-hosted front-end for the Bullseye store locator.
See http://www.bullseyelocations.com/ for further details on Bullseye, and to
sign up for an account. This module requires that the Location module
(http://drupal.org/project/location) also be installed & enabled. We also
require that your PHP install has cURL support.

Go to admin/config/services/bullseye to configure the store locator.
Once configured, the store locator can be found at /storelocator.
